import java.util.Scanner;

public class Task13 {
        public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);

            System.out.print("Enter first number: ");
            int num1 = scanner.nextInt();

            System.out.print("Enter second number: ");
            int num2 = scanner.nextInt();

            char[] operators = {'+', '-', '*', '/', '%'};
            int[] results = new int[5];

            for (int i = 0; i < operators.length; i++) {
                switch (operators[i]) {
                    case '+':
                        results[i] = num1 + num2;
                        break;
                    case '-':
                        results[i] = num1 - num2;
                        break;
                    case '*':
                        results[i] = num1 * num2;
                        break;
                    case '/':
                        if (num2 == 0) {
                            System.out.println("Error: Division by zero.");
                            return;
                        }
                        results[i] = num1 / num2;
                        break;
                    case '%':
                        results[i] = num1 % num2;
                        break;
                    default:
                        System.out.println("Error: Invalid operator.");
                        return;
                }
            }

            System.out.println(num1 + " + " + num2 + " = " + results[0]);
            System.out.println(num1 + " - " + num2 + " = " + results[1]);
            System.out.println(num1 + " * " + num2 + " = " + results[2]);
            System.out.println(num1 + " / " + num2 + " = " + results[3]);
            System.out.println(num1 + " mod " + num2 + " = " + results[4]);
        }
    }
