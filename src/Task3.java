import java.util.Scanner;

//3)Daxil edilən ədədin bütün rəqəmlərinin cəmini tapan proqram yazın.
public class Task3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Please, enter a number: ");
        int number = input.nextInt();
        System.out.println("Sum of digits is " + sumOfDigits(number));
    }

    public static int sumOfDigits (int num){
        int sum = 0;
        while(num!=0){
            int digit = num%10;
            sum+=digit;
            num/=10;
        }
        return sum;
    }
}
