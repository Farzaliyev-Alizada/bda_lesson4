import java.util.Scanner;

//8)Rekursiyadan istifadə edərək factorial hesablayan proqram tərtib edin.
public class Task8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Bir reqem daxil edin: ");
        int num = input.nextInt();
        System.out.println(num + "!"+" = "+factorial(num));
    }

    public static int factorial(int n){
        if(n==0){
            return 1;
        }
        else{
            return n* factorial(n-1);
        }

    }
}
