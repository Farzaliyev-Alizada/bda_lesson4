/*
10) Write a Java program to divide two numbers and print on the screen.
        Test Data :
        50/3
        Expected Output :
        16
*/

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Birinci ededi daxil edin: ");
        double num1= input.nextDouble();
        System.out.println("Ikinci ededi daxil edin: ");
        double num2 = input.nextDouble();
        int result = (int) (num1/num2);
        System.out.println(result);
    }


}
