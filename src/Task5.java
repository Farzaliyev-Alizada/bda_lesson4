import java.util.Scanner;

//5)Daxil edilmiş sətirin tərsi ilə düzünün eyni olub olmadığını yoxlayan proqram yazın.
public class Task5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Soz daxil edin");
        String word = input.next();
        boolean reverseWord = findReverse(word);
        if(reverseWord){
            System.out.println(word+" sozunun tərsi ilə düzü eynidir");
        } else {
            System.out.println(word+" sozunun tərsi ilə düzü eyni deyil");

        }
    }

    public static boolean findReverse (String str){
        String reverseStr= new StringBuilder(str).reverse().toString();
        return str.equals(reverseStr);
    }
}
