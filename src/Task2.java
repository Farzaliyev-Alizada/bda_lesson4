import java.util.Scanner;

/*1)Daxil edilmiş ədədə qədər bütün tek ədədləri çap edən proqram yazın.*/
public class Task2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Please, enter a number: ");
        int number = input.nextInt();
        evenNumbers(number);
    }

    public static int evenNumbers(int num) {
        for (int i=0; i<num;i++){
            if(i%2!=0){
                System.out.println(i);
            }
        }
        return num;
    }
}

