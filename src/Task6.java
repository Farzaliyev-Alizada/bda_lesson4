import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

//6)Daxil edilən sətirdə hansı simvoldan neçə dəfə istifadə olunduğunu çap edən proqram yazın.
public class Task6 {
    private static Set<Character> processedChars = new HashSet<>();
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Soz daxil edin");
        String word = input.nextLine();
        findSameCh(word);
    }
    public static void findSameCh(String str){

        for (int i = 0; i < str.length() ; i++) {
         char ch1 = str.charAt(i);
         int count =0;

            if ((ch1 == ' ') || processedChars.contains(ch1)) continue;
            for (int j = 0; j < str.length(); j++) {
              char ch2 = str.charAt(j);
               if(ch1==ch2){
                   count++;
               }
            }
            System.out.println(ch1 + ": " + count);
            processedChars.add(ch1);
        }

//
    }
}
