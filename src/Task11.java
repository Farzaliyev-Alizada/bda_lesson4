import java.util.Scanner;

public class Task11 {
    public static void main(String[] args) {

        int result = (int)performManyOperations();
        System.out.println("Result: " + result);


    }
    public static double performManyOperations() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the expression: ");
        String input = scanner.nextLine();

        // Split the input into separate tokens
        String[] tokens = input.split(" ");

        // Initialize the result to the first number
        double result = Double.parseDouble(tokens[0]);

        // Apply the remaining operators to the result
        for (int i = 1; i < tokens.length; i += 2) {
            char operator = tokens[i].charAt(0);
            double num = Double.parseDouble(tokens[i + 1]);

            // Handle operator precedence
            switch (operator) {
                case '*':
                    result *= num;
                    break;
                case '/':
                    if (num == 0) {
                        System.out.println("Error: Division by zero.");
                        return 0;
                    }
                    result /= num;
                    break;
                case '%':
                    if (num == 0) {
                        System.out.println("Error: Modulus by zero.");
                        return 0;
                    }
                    result %= num;
                    break;
                case '+':
                    // If the next operator is multiplication or division,
                    // evaluate that first before adding to the result
                    if (i + 2 < tokens.length && (tokens[i + 2].charAt(0) == '*' || tokens[i + 2].charAt(0) == '/')) {
                        double nextNum = Double.parseDouble(tokens[i + 3]);
                        if (tokens[i + 2].charAt(0) == '*') {
                            result += num * nextNum;
                        } else {
                            result += num / nextNum;
                        }
                        i += 2; // Skip over the next operator and number
                    } else {
                        result += num;
                    }
                    break;
                case '-':
                    // If the next operator is multiplication or division,
                    // evaluate that first before subtracting from the result
                    if (i + 2 < tokens.length && (tokens[i + 2].charAt(0) == '*' || tokens[i + 2].charAt(0) == '/')) {
                        double nextNum = Double.parseDouble(tokens[i + 3]);
                        if (tokens[i + 2].charAt(0) == '*') {
                            result -= num * nextNum;
                        } else {
                            result -= num / nextNum;
                        }
                        i += 2; // Skip over the next operator and number
                    } else {
                        result -= num;
                    }
                    break;
                default:
                    System.out.println("Error: Invalid operator.");
                    return 0;
            }
        }

        return result;
    }

}


