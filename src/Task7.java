import java.util.Scanner;

//7)Daxil edilmiş sətirdə neçə sait və neçə samit olduğunu tapan proqram tərtib edin.
public class Task7 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Söz daxil edin: ");
        String word = input.nextLine().toLowerCase();
        countLetter(word);
    }

    public static void countLetter(String str) {
        char[] saitler = new char[]{'a', 'ı', 'o', 'u', 'e', 'ə', 'i', 'ö', 'ü'};
        int countSait = 0;
        int countSamit = 0;
        for (int i = 0; i < str.length(); i++) {
            char letter = str.charAt(i);
            boolean isSait = false;
            for (int j = 0; j < saitler.length; j++) {

                if (letter == saitler[j]) {
                    countSait++;
                    isSait = true;
                    break;
                }
            }
            if (!isSait) {
                countSamit++;

                System.out.println(letter);
            }
        }
        System.out.println("Daxil edilmish sozde cemi sait sayi - " + countSait + ", cemi samit sayi ise " + countSamit);
    }

}

