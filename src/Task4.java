//4)Daxil edilən ədədin sadə yoxsa mürəkkəb olduğunu tapan proqram yazın. (Scanner, for, şərt)
import java.util.Scanner;

    public class Task4 {
        public static void main(String[] args) {
            Scanner input = new Scanner(System.in);

            System.out.println("Please, enter a positive integer: ");
            int number = input.nextInt();

            if (isPrime(number)) {
                System.out.println(number + " is a prime number.");
            } else {
                System.out.println(number + " is a composite number.");
            }
        }

        public static boolean isPrime(int num) {
            if (num <= 1) {
                return false;
            }

            for (int i = 2; i <= Math.sqrt(num); i++) {
                if (num % i == 0) {
                    return false;
                }
            }

            return true;
        }
    }


